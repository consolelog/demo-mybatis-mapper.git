# AngularJS + SpringBoot Demo

- [AngularJS](https://angularjs.org/)
- [Spring Boot](https://spring.io/projects/spring-boot)
- [MyBatis](https://mybatis.org/mybatis-3)
- [Mapper](https://gitee.com/free/Mapper)
- [PageHelper](https://pagehelper.github.io/)