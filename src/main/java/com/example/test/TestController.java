package com.example.test;

import com.example.generator.mapper.UserMapper;
import com.example.generator.model.User;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangbin
 */
@RestController
public class TestController {
    @Resource
    UserMapper userMapper;

    @GetMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @GetMapping("/user")
    public PageInfo<User> selectAll(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<User> users = userMapper.selectAll();
        return new PageInfo<>(users);
    }

    @PostMapping("/user")
    public void insertSelective(@RequestBody User user) {
        userMapper.insertSelective(user);
    }

    @PutMapping("/user")
    public void updateByPrimaryKeySelective(@RequestBody User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }

    @DeleteMapping("/user/{id}")
    public void deleteByPrimaryKey(@PathVariable Long id) {
        userMapper.deleteByPrimaryKey(id);
    }
}

