package com.example.generator.mapper;

import com.example.generator.model.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}