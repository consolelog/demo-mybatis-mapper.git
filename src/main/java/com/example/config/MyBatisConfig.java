package com.example.config;

import org.springframework.context.annotation.Configuration;

/**
 * @author wangbin
 */
@Configuration
@tk.mybatis.spring.annotation.MapperScan("com.example.**.mapper")
public class MyBatisConfig {
}
